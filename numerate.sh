#! /bin/bash
#
# numerate.sh creates and updates identification files for the Series
# 1 3D printer image files it is intended to be run on a system with
# an sdcard image containing a BBB filesystem, mounted for editing
#

# to be unleashed, numerate needs to eventually:
# 0) introduce itself to the user
# 1) list removable devices driven by sd or by mmc,
# 2) allow the user/operater of said script to choose a rootfs
# partition from one of the listed devices
# 3) perform the updates to ID files on said drive.

# it is fully possible that writing this script is a waste of time and
# that a script from elinux.org, rcn, or elsewhere should be modified
# or extended to suit our needs.

set -e

# customer login
username="ubuntu"

octoprint_git_rev="v1.1.0a4"
ubuntu_release="precise"

git_version=$(git describe --dirty)
if [[ $git_version = *-dirty ]]; then
	echo "Code contains changes that aren't checked in:"
	git status --short
	exit 1
fi
echo "image builder version: $git_version"

overwrite=0

while :; do
    case $1 in
		--overwrite)
			overwrite=1
			;;
        --)						# End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *) # Default case: If no more options then break out of the loop.
            break
    esac

    command shift
done

if [ "$UID" -ne 0 ]
then echo "Numerate Error: Please run as root"
	exit
fi

EssDee=$(
	for device in /dev/mmcblk?p2; do
		file -sk "$device" | grep ext | cut -d":" -f1 ;
	done
)

# sorry, but this test sucks.  hardly a test for an SD card.  some
# ideas: assume udev on the host system and use udevadm or walk the
# filesystem for info on the attached drives that could host usable
# partitions
EssDeeTest () {
    if [ "${2}" ] ; then
        echo "Numerate Error: Two or more potential destinations, please remove all but one SD card"
        echo ""
        exit 1
    fi
    if [ ! "${1}" ] ; then
        echo "Numerate Error: No potential destinations, please insert one SD card"
        echo ""
        exit 1
    fi
}

EssDeeTest $EssDee

mkdir -p /mnt/rootfs
#target=/media/qc/rootfs/
#target changed for ease of use, to be corrected once mounting is automatic
target=/mnt/rootfs
if ! mount $EssDee /mnt/rootfs; then
    echo "Try 'sudo umount $EssDee'"; exit 1
fi

expected_old_hostname="series1-(unconfigured|image|1000)"
if ! egrep -q "$expected_old_hostname" $target/etc/hostname; then
	echo "Based on /etc/hostname, this doesn't look like an unconfigured image."
	if [ $overwrite = 1 ]; then
		echo "Overwriting"
	else
		exit 1
	fi
fi

#mount -a

#mount `tune2fs -l `for device in /dev/sd?2; do file -sk $device | grep ext | cut -d":" -f1 ; done` | grep name | cut -d" " -f6` $target 

echo Please enter the serial number or name of the machine

read serial

echo "$serial" > $target/etc/unit

home="${target}/home/${username}"

# record the OS version
m4 -DVERSION="$git_version" -DRELEASE="$ubuntu_release" -Ifiles \
	files/os-release.in > files/os-release
install --owner root --group root --mode="a=r" files/os-release $target/etc/

# add octoprint config
cp -r "files/series 1" "${home}/.series 1"

# remove any memory of previous wifi adaptors
rm -f "${target}/etc/udev/rules.d/70-persistent-net.rules"
# don't automatically generate names for new net devices
rm -f $target/lib/udev/rules.d/75-persistent-net-generator.rules
# rename ralink devices wlan0
cp files/70-persistent-net.rules $target/etc/udev/rules.d/.
cp files/forgetsafe.conf $target/etc/init/.
echo "manual" > $target/etc/init/failsafe.override
# set dhcp timeouts
install files/dhclient.conf $target/etc/dhcp/ --owner=root --mode='-rw-r--r--'
# network settings files
install files/interfaces "$target/etc/network/" \
    --owner=root --group=dialout --mode="ug=rw,o=-"
install files/interfaces.template "$target/etc/network/" \
    --group=dialout --mode="ug=r,o=-"

# make sure we have unique ssh host keys
rm $target/etc/ssh/*key*
ssh-keygen -t dsa -N "" -f $target/etc/ssh/ssh_host_dsa_key > /dev/null
ssh-keygen -t rsa -N "" -f $target/etc/ssh/ssh_host_rsa_key > /dev/null
ssh-keygen -t ecdsa -N "" -f $target/etc/ssh/ssh_host_ecdsa_key > /dev/null

# set the hostname based on the hardware serial number
hostname="series1-$serial"
echo "$hostname" > $target/etc/hostname
cp $target/etc/hosts $target/etc/hosts.old
sed "s/$expected_old_hostname/$hostname/g" \
	$target/etc/hosts.old > $target/etc/hosts
if [ ! $? ]; then
	echo "Failed to edit hosts file." 1>&2
	exit 1
fi
rm $target/etc/hosts.old

# export octoprint from git
octoprint_home="${home}/octoprint-type-a"
(
	cd ../octoprint-type-a
	rm -rf "$octoprint_home"
	git archive --prefix=octoprint-type-a/ "$octoprint_git_rev" \
		| tar -xp -C "$home"
)

umount $target

echo "$serial" >> ~/serials

echo "numerate: done."
